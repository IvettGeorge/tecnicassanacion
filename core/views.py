import code
import re
from django.forms import forms
from django.http.response import HttpResponseRedirect
from django.shortcuts import render

from django.http import HttpResponse, JsonResponse, request
from django.urls.base import is_valid_path
from requests.api import get
from core.models import *

from django.views.generic import ListView, CreateView

from core.models import *
from core.forms import *

from analytics.views import *

from user.views import *
from django.contrib.auth import get_user_model

from django.urls import reverse_lazy
from django.conf import settings
# Create your views here.

def firtsTemplate(request):
    dictionary={
        'usuario': 'IvanRl_29'
    }
    return render(request,'index.html',dictionary)

def agenda(request):
    dictionary={
        'usuario': 'IvanRl_29'
    }
    return render(request,'agenda.html',dictionary)

class agendaListView(ListView):
    model=Agenda
    template_name='agenda.html'

    def get(self, request, *args, **kwargs) -> HttpResponse:
        if not request.user.is_authenticated:
            print("Login")
            print(request.user)
            return redirect('/login')
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context= super().get_context_data(**kwargs)
        context['actual']=datetime.now()
        context['nameEvent']="Demo Sitacomm"
        context['dateEvent']="12 de Noviembre del 2021"

        return context

class agendaTest(ListView):
    model=Agenda
    template_name='agendaprimera.html'

    def get_context_data(self, **kwargs):
        context= super().get_context_data(**kwargs)
        context['actual']=datetime.now()
        return context

class inicioListView(ListView):
    model=Agenda
    template_name='inicio.html'
    analytics={}
    code=settings.CODE_EVENT
    
    def get(self, request, *args, **kwargs) -> HttpResponse:
        if not request.user.is_authenticated:
            print("Login")
            print(request.user)
            return redirect('/login')
        try:
            self.analytics=getAnalytics(request)
            print("Analytics Captured")
            self.analytics=json.dumps(self.analytics)
        except:
            print("Sin Analytics")
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context= super().get_context_data(**kwargs)
        context['title']='title'
        context['formQuestion']=QuestionForm
        context['actionQuestion']='add'
        context['analytics']=self.analytics
        context['code']=self.code
        return context

class QuestionCreateView(CreateView):
    model=Question
    form_class=QuestionForm
    template_name='inicio.html'
    code=settings.CODE_EVENT

    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        return render(request, self.template_name,self.code)
    
    def post(self, request, *args, **kwargs):
        data={}
        try:
            action=request.POST['action']
            if action == 'add':
                form=self.get_form()
                if form.is_valid():
                    form.save(True)
                    print("Guardada")
                else:
                    data['error']=form.errors
            else:
                data['error']='Error al enviar la pregunta'
        except Exception as e:
            data['error']=str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):        
        context= super().get_context_data(**kwargs)
        context['title']='Crear Question'
        context['action'] = 'add'
        return context

class QuizzCreateView(CreateView):
    model=Trivia
    form_class=TrviaForm
    template_name='inicio.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        return render(request, self.template_name)
    
    def post(self, request, *args, **kwargs):
        data={}
        try:
            action=request.POST['action']
            if action == 'add':
                form=self.get_form()
                if form.is_valid():
                    form.save(True)
                    answers=getAnswers2(request.POST['question'])
                    data['answers']=answers
                else:
                    data['error']=form.errors
            else:
                data['error']='Error al enviar la pregunta'
        except Exception as e:
            data['error']=str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):        
        context= super().get_context_data(**kwargs)
        context['title']='Crear Question'
        context['action'] = 'add'
        return context

def getAnswers(request):
    data={}
    try:
        action=request.POST['action']
        if action == 'add':
            quizz=Trivia(
                question=request.POST['question'],
                answer=request.POST['answer'],
                user=request.POST['user'],
                mailUser=request.POST['mailUser'],
                score=request.POST['score']
            )
            quizz.save()
            answers=getAnswers2(request.POST['question'])
            data['answers']=answers['data']
            data['total']=answers['total']
        else:
            data['error']='Error al enviar la pregunta'    
    except Exception as e:
        data['error']=str(e)
    return JsonResponse(data)
    
def getAnswers2(idQuizz):
    data={}
    trivias=Trivia.objects.filter(question=idQuizz)
    answers=[]
    for trivia in trivias:
        answers.append(trivia.answer)
    a=answers.count('A')
    b=answers.count('B')
    c=answers.count('C')
    d=answers.count('D')
    total=a+b+c+d
    data={'A':a,'B':b,'C':c,'D':d}
    context={'data':data,'total':total}
    return context

class TriviaCreateView(CreateView):
    model=Trivia
    form_class=QuestionForm
    template_name='inicio.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        return render(request, self.template_name)
    
    def post(self, request, *args, **kwargs):
        data={}
        try:
            action=request.POST['action']
            if action == 'add':
                form=self.get_form()
                if form.is_valid():
                    form.save(True)
                    print("Guardada")
                else:
                    data['error']=form.errors
            else:
                data['error']='Error al enviar la pregunta'
        except Exception as e:
            data['error']=str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):        
        context= super().get_context_data(**kwargs)
        context['title']='Crear Question'
        context['action'] = 'add'
        return context

class SurveyCreateView(CreateView):
    model=Survey
    form_class=SurveyForm
    template_name='inicio.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        return render(request, self.template_name)
    
    def post(self, request, *args, **kwargs):
        data={}
        try:
            action=request.POST['action']
            if action == 'add':
                form=self.get_form()
                if form.is_valid():
                    form.save(True)
                    print("Guardada")
                else:
                    data['error']=form.errors
                    print(form.errors)
            else:
                data['error']='Error al enviar la pregunta'
        except Exception as e:
            data['error']=str(e)
            print(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):        
        context= super().get_context_data(**kwargs)
        context['title']='Crear Question'
        context['action'] = 'add'
        return context

class SpeakerCreateView(CreateView):
    model=Speaker
    form_class=SpeakerForm
    template_name='speakerc.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        return render(request, self.template_name)
    
    def post(self, request, *args, **kwargs):
        data={}
        try:
            action=request.POST['action']
            if action == 'add':
                nameC=request.POST.get('name')
                descriptionC=request.POST.get('description')
                jobC=request.POST.get('job')
                imgProfileC=request.FILES.get('imgProfile')
                speaker=Speaker(name=nameC,description=descriptionC,job=jobC, imgProfile=imgProfileC).save()
                #form=self.get_form()
                #print(form)
                #if form.is_valid():
                #    form.save(True)
                #    print("GuardadO")
                #else:
                #    data['error']=form.errors
            else:
                data['error']='Error al enviar la pregunta'
        except Exception as e:
            data['error']=str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):        
        context= super().get_context_data(**kwargs)
        context['title']='Crear Speaker'
        context['action'] = 'add'
        return context

class QuestionExter(CreateView):
    model=Question
    form_class=QuestionForm
    template_name='questioner.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        return render(request, self.template_name)
    
    def post(self, request, *args, **kwargs):
        data={}
        try:
            action=request.POST['action']
            if action == 'add':
                form=self.get_form()
                if form.is_valid():
                    form.save(True)
                    print("GuardadO")
                else:
                    data['error']=form.errors
            else:
                data['error']='Error al enviar la pregunta'
        except Exception as e:
            data['error']=str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):        
        context= super().get_context_data(**kwargs)
        context['title']='Crear Speaker'
        context['action'] = 'add'
        return context

class speakerListView(ListView):
    model=Speaker
    template_name='speaker.html'

    def get(self, request, *args, **kwargs) -> HttpResponse:
        if not request.user.is_authenticated:
            print("Login")
            print(request.user)
            return redirect('/login')
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context= super().get_context_data(**kwargs)
        context['actual']=datetime.now()
        context['nameEvent']="Speakers"

        return context

class questionListView(ListView):
    model=Question
    template_name='question.html'

    def get(self, request, *args, **kwargs) -> HttpResponse:
        if not request.user.is_authenticated:
            print("Login")
            print(request.user)
            return redirect('/login')
        return super().get(request, *args, **kwargs)


    def get_context_data(self, **kwargs):
        questions=Question.objects.filter(code=settings.CODE_EVENT)
        context= super().get_context_data(**kwargs)
        context['nameEvent']="Preguntas al ponenete"
        context['questions']=questions

        return context

class meet1ListView(ListView):
    model=Agenda
    template_name='indexMeet.html'

    def get_context_data(self, **kwargs):
        context= super().get_context_data(**kwargs)
        context['nameEvent']="Demo Sitacomm"
        context['dateEvent']="12 de Noviembre del 2021"

        return context

class meet2ListView(ListView):
    model=Agenda
    template_name='meet.html'

    def get_context_data(self, **kwargs):
        context= super().get_context_data(**kwargs)
        context['nameEvent']="Demo Sitacomm"
        context['dateEvent']="12 de Noviembre del 2021"
        
        return context
    
def meet2(request,name, mn, email, pwd,role, lang, signature, china, apiKey):
    meetParams={
        'name': name,
        'mn':mn,
        'email':email,
        'pwd':pwd,
        'role':role,
        'lang':lang,
        'signature':signature,
        'china':china,
        'apiKey':apiKey
    }

    return render(request,'meet.html',meetParams)

def breakouts(request):    
    if not request.user.is_authenticated:
        print("Login")
        print(request.user)
        return redirect('/login')
    context={
        "tittle":"Breakouts"
    }
       
    return render(request,'breakouts.html',context)

class loginListView(ListView):
    model=Agenda
    template_name='loginlib.html'

    def get_context_data(self, **kwargs):
        context= super().get_context_data(**kwargs)
        context['actual']=datetime.now()

        return context

def getQuiz(request):
    quizz=Quizz.objects.filter(name='quizz')
    for quiz in quizz:
        quizzNumber=quiz.numberActive
    return JsonResponse({'data':quizzNumber})

def getSurveyStatus(request):
    surveys=Quizz.objects.filter(name='survey')
    for survey in surveys:
        surveyNumber=survey.numberActive
    return JsonResponse({'data':surveyNumber})

def getOnline(request,code):
    users=getOnlineUserEvent(code)
    if users:
        dictionary={
        'estado': '200',
        'usersOnline':users      
    }
    else:
        dictionary={
            'estado': 'Error de usuarios'       
        }
    return JsonResponse(dictionary)  

def getQuestionsEvent(codeEvent):
    questions=Question.objects.filter(code=codeEvent)
    questionsList = []
    for question in questions:
        questonDict={'question':question.question,
        'nombre':question.user,'email':question.email,
        'fecha':question.date_created,
        'code':question.code
        }
        questionsList.append(questonDict) 
    return questionsList

def getQuestions(request,code):
    questions=getQuestionsEvent(code)
    if questions:
        dictionary={
        'estado': '200',
        'questions':questions      
    }
    else:
        dictionary={
            'estado': 'Error al obtener preguntas'       
        }
    return JsonResponse(dictionary)  

def getRegisterEvent(codeEvent):
    registrados=Event_User.objects.filter(code=settings.CODE_EVENT)
    registradosList = []
    for registrado in registrados:
        registradosDict={'question':registrado.code,
        'email':registrado.mail,
        'registro':registrado.date_created,
        'eventoM':registrado.id_eventSS
        }
        registradosList.append(registradosDict) 
    return registradosList

def getRegister(request,code):
    register=getRegisterEvent(code)
    if register:
        dictionary={
        'estado': '200',
        'registrados':register      
    }
    else:
        dictionary={
            'estado': 'Error al obtener registrados'       
        }
    return JsonResponse(dictionary)  

def getConnected(request):
    xd=getOnlineUserEvent2(settings.CODE_EVENT)
    dictionary={
        'usuario': ''
    }
    return render(request,'index.html',dictionary)
    

def getOnlineUserEvent2(codeEvent):
    active_sessions = Session.objects.filter(expire_date__gte=timezone.now())
    user_id_list = []
    for session in active_sessions:
        data = session.get_decoded()
        user_id_list.append(data.get('_auth_user_id', None))
    
    usersEvents=Event_User.objects.filter(code=codeEvent)
    user_event_list=[]    
    for user in usersEvents:
        user_event_list.append(user.mail)    
    return user_event_list