#from demo.core.models import Agenda, Speaker, Trivia, Survey
from core.models import Agenda, Speaker, Trivia, Survey
from django.contrib import admin

from core.models import Agenda
# Register your models here.
admin.site.register(Agenda)
admin.site.register(Speaker)
admin.site.register(Trivia)
admin.site.register(Survey)

