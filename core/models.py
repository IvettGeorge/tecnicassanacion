from django.db import models

# Create your models here.

from django.db import models
from datetime import datetime
# libs path_rename
from django.utils import timezone
import os
from uuid import uuid4
from django.utils.timezone import now
from user.models import User

#Name Speakers
def path_and_rename(instance, filename):
    upload_to = 'photos'
    ext = filename.split('.')[-1]
    # get filename
    if instance.pk:
        filename = '{}.{}'.format(instance.pk, ext)
    else:
        # set filename as random string
        filename = '{}.{}'.format(uuid4().hex, ext)
    # return the whole path to the file
    return os.path.join(upload_to, filename)

# Create your models here.
class Speaker(models.Model):
    name=models.CharField(max_length=150, verbose_name='Name')
    job=models.CharField(max_length=150,verbose_name='Job')
    imgProfile=models.ImageField(upload_to='speakers/')
    description=models.CharField(max_length=250, verbose_name='Description')
    date_created=models.DateTimeField(auto_now=True)
    date_update=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table='speaker'
        verbose_name='Speaker'
        verbose_name_plural='Speaker'
        ordering=['id']

class Agenda(models.Model):
    name=models.CharField(max_length=150, verbose_name='Nombre')
    date=models.DateField(verbose_name='Fecha')
    startTime=models.TimeField(verbose_name='Inicio')
    endTime=models.TimeField(verbose_name='Fin')
    description=models.CharField(max_length=150, verbose_name='Descripción')

    speaker=models.ForeignKey(Speaker, on_delete=models.CASCADE )
    date_created=models.DateTimeField(auto_now=True)
    date_update=models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.name
    
    class Meta:
        db_table='agenda'

class Trivia(models.Model):
    question=models.CharField(max_length=150, verbose_name='question')
    answer=models.CharField(max_length=150,verbose_name='Answer')
    user=models.CharField(max_length=150,verbose_name='Name')
    mailUser=models.CharField(max_length=150,verbose_name='Mail')
    score=models.IntegerField()
    code=models.CharField(max_length=150, verbose_name='codigo', blank=True)
    date_created=models.DateTimeField(auto_now=True)
    date_update=models.DateTimeField(auto_now_add=True)
    

    class Meta:
        db_table='trivia'
        verbose_name='Trivia'
        verbose_name_plural='Trivias'
        ordering=['id']
        
class Survey(models.Model):
    user=models.CharField(max_length=150,verbose_name='User',null=True)
    email=models.CharField(max_length=150,verbose_name='Email',null=True)
    s1=models.CharField(max_length=150,verbose_name='Answer 1', null=True)
    s2=models.CharField(max_length=150,verbose_name='Answer 2',null=True)
    s3=models.CharField(max_length=150,verbose_name='Answer 3',null=True)
    code=models.CharField(max_length=150, verbose_name='codigo', blank=True)

    date_created=models.DateTimeField(auto_now=True)
    date_update=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user

    class Meta:
        db_table='survey'
        verbose_name='Survey'
        verbose_name_plural='Surveys'
        ordering=['id']

class Question(models.Model):
    question=models.CharField(max_length=250, verbose_name='Question')
    user=models.CharField(max_length=100, verbose_name='user')
    email=models.EmailField(verbose_name="Email")
    date_created=models.DateTimeField(auto_now=True)
    date_update=models.DateTimeField(auto_now_add=True)
    code=models.CharField(max_length=50,verbose_name='Code Event',blank=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table='questions'
        verbose_name='Question'
        verbose_name_plural='Question'
        ordering=['id']

class Event(models.Model):
    name=models.CharField(max_length=50, verbose_name='Nombre del Evento')
    idStar=models.IntegerField(verbose_name='Id StarTicket')
    code=models.CharField(max_length=50, verbose_name='Código')
    start=models.DateTimeField(auto_now=True, verbose_name='Inicio')
    end=models.DateTimeField(auto_now=True, verbose_name='Fin')
    date_created=models.DateTimeField(auto_now=True)
    date_update=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name
    
    class Meta:
        db_table='Evento'

class Event_User(models.Model):
    id_eventSS=models.CharField(max_length=50, verbose_name='ID del Evento SS')
    id_eventDS=models.CharField(max_length=50, verbose_name='ID del Evento Demo')
    code=models.CharField(max_length=50, verbose_name='Código del evento')
    idUserSS=models.CharField(max_length=50, verbose_name='ID del usuario SS')
    idUserDS=models.CharField(max_length=50, verbose_name='ID del usuario Demo')
    mail=models.EmailField(verbose_name="Email")
    event=models.ForeignKey(Event, on_delete=models.CASCADE)
    user=models.ForeignKey(User, on_delete=models.CASCADE)
    date_created=models.DateTimeField(auto_now=True)
    date_update=models.DateTimeField(auto_now_add=True)
    
   
    class Meta:
        db_table='user_event'

class Quizz(models.Model):
    name=models.CharField(max_length=150, verbose_name='name')
    numberActive=models.IntegerField()

    date_created=models.DateTimeField(auto_now=True)
    date_update=models.DateTimeField(auto_now_add=True)
    
    class Meta:
        db_table='Active'
        verbose_name='Active'
        verbose_name_plural='Actives'
        ordering=['id']
    
