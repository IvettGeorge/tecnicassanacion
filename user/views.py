from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.urls.base import is_valid_path
from django.views.generic import ListView, CreateView
from user.forms import UserForm, EventForm, EventUserForm
from user.models import *
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from core.models import *
from user.models import User

import json
from django.contrib.auth import get_user_model

from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from django.utils import timezone
from django.conf import settings

# Create your views here.

class UserCreateView(CreateView):
    model=User
    form_class=UserForm
    template_name='register.html'
    success_url='inicio2'
    permission_required='user.add_user'
    url_redirect=success_url

    #@method_decorator(csrf_exempt)
    #def dispatch(self, request, *args, **kwargs):
    #    return super(UserCreateView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):        
        context= super().get_context_data(**kwargs)
        context['title']='Registro'
        context['action'] = 'add'
        return context

    def post(self, request, *args, **kwargs):
        data={}
        try:
            action=request.POST['action']
            print("Accion")
            print(action)
            if action == 'add':
                form=self.get_form()
                if form.is_valid():  
                    print("a")                  
                    form.save(True)
                else:
                    data['error']=form.errors
            else:
                data['error']='Error al registrar usuario'
        except Exception as e:
            data['error']=str(e)
        print(data)
        return JsonResponse(data)

class UserApiCreateview(CreateView):
    model=User
    form_class=UserForm
    template_name='register.html'
    success_url='inicio2'
    permission_required='user.add_user'
    url_redirect=success_url

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(UserApiCreateview, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data={}
        userData = json.loads(request.body)
        try:
            action=userData['action']
            if action == 'add':
                print(self)
                form=UserForm(userData)
                if form.is_valid():  
                    print("a")                  
                    form=form.save(True)
                    data['mensaje']='Usuario Creado Correctamente'
                    data['id_user_event_system']=form.id                    
                    data['error']="200"
                else:
                    data['error']=form.errors
            else:
                data['error']='Error al registrar usuario'
        except Exception as e:

            data['error']=str(e)
        print(data)
        return JsonResponse(data)
 
class EventApiCreateview(CreateView):
    model=Event
    form_class=EventForm

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(EventApiCreateview, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data={}
        eventData = json.loads(request.body)
        try:
            action=eventData['action']
            if action == 'add':
                print(self)
                form=EventForm(eventData)
                print(form)
                if form.is_valid():                                      
                    form=form.save(True)
                    data['mensaje']='Evento Creado Correctamente'
                    data['id_event_event_system']=form.id                    
                    data['error']="200"
                else:
                    data['error']=form.errors
            else:
                data['error']='Error al registrar evento'
        except Exception as e:
            data['error']=str(e)

        print(data)
        return JsonResponse(data)

class EventUserApiCreateview(CreateView):
    model=Event_User
    form_class=EventUserForm

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(EventUserApiCreateview, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data={}
        eventData = json.loads(request.body)
        try:
            action=eventData['action']
            if action == 'add':
               
                form=EventUserForm(eventData)
                #print(form)
                if form.is_valid():                                      
                    form=form.save(True)
                    data['mensaje']='Acesso al evento Creado Correctamente'
                    data['id_accessuserevent_event_system']=form.id                    
                    data['error']="200"
                else:
                    data['error']=form.errors
            else:
                data['error']='Error al registrar acceso al evento'
        except Exception as e:
            data['error']=str(e)

        print(data)
        return JsonResponse(data)

class UserApiUpdateView(CreateView):
    model=User
    #form_class=UserForm

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(UserApiUpdateView, self).dispatch(request, *args, **kwargs)


    def post(self, request, *args, **kwargs):
        data={}
        userData = json.loads(request.body)
        try:
            action=userData['action']
            if action == 'update':
               
                user = User.objects.get(pk=userData['id_user_event_system'])
                #print(user)
                user.set_password(userData['password'])                
                user.save()             
                data['id_user_event_system']=user.id               
                data['mensaje']='Usuario Actualizado Correctamente'                               
                data['error']="200"
              
            else:
                data['error']='Error al actualizar password usuario'
        except Exception as e:

            data['error']=str(e)
        print(data)
        return JsonResponse(data)

class EventApiUpdateview(CreateView):
    model=Event
    form_class=EventForm

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(EventApiUpdateview, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data={}
        eventData = json.loads(request.body)
        try:
            action=eventData['action']
            if action == 'update':
               
                
                event = Event.objects.get(pk=eventData['id_event_system'])
                #print(user)
                event.code= eventData['code']      
                event.idStar= eventData['idStar'] 
                event.name= eventData['name'] 
                #event.start= eventData['start'] 
                #event.end= eventData['end'] 
                event.save()             
                data['id_event_event_system']=event.id               
                data['mensaje']='Evento Actualizado Correctamente'                               
                data['error']="200"

            else:
                data['error']='Error al registrar evento'
        except Exception as e:
            data['error']=str(e)

        print(data)
        return JsonResponse(data)

def get_current_users():
    active_sessions = Session.objects.filter(expire_date__gte=timezone.now())
    user_id_list = []
    for session in active_sessions:
        data = session.get_decoded()
        print("Data: ")
        print(data)
        user_id_list.append(data.get('_auth_user_id', None))

    usersEvents=Event_User.objects.all()
    print("USers-event")
    print(usersEvents)

    user_event_list=[]

    for user in usersEvents:
        user_event_list.append(user.idUserSS)        
    print(user_id_list)
    print(user_event_list)

    # Query all logged in users based on id list
    UserModel=get_user_model()
    usersl=UserModel.objects.filter(id__in=user_id_list)
    return usersl


def getOnlineUserEvent(codeEvent):
    active_sessions = Session.objects.filter(expire_date__gte=timezone.now())
    user_id_list = []
    for session in active_sessions:
        data = session.get_decoded()
        user_id_list.append(data.get('_auth_user_id', None))
    
    usersEvents=Event_User.objects.filter(code=codeEvent)
    user_event_list=[]    
    for user in usersEvents:
        user_event_list.append(user.mail)    
    
    UserModel=get_user_model()
    conected=UserModel.objects.filter(id__in=user_id_list,email__in=user_event_list)    
    email_list=[]    
    for email in conected:       
        email_list.append(email.email)   
        print(email_list)
    return email_list