popupWhatsApp = () => {
  
  let btnClosePopup = document.querySelector('.closePopup');
  let btnOpenPopup = document.querySelector('.whatsapp-button');
  let popup = document.querySelector('.popup-whatsapp');
  let sendBtn = document.getElementById('send-btn');
  
  popup.style.display = "none";

  btnClosePopup.addEventListener("click",  () => {
     popup.style.display = "none";
  })
  
  btnOpenPopup.addEventListener("click",  () => {
     popup.style.animation = "fadeIn .6s 0.0s both";
     popup.style.display = "block";
  })
  

  
  sendBtn.addEventListener("click", () => {
   let msg = document.getElementById('whats-in').value;
   let relmsg = msg.replace(/ /g,"%20");
     
   window.open('https://wa.me/5215543775641?text='+relmsg, '_blank'); 
  
  });

}

popupWhatsApp();