'use-strict'
const ASW = require('aws-sdk');
const documentClient = new ASW.DynamoDB.DocumentClient();


exports.handler = async (event, context) => {
    
    //await createMessageMock();
    //return ;
    //const documentClient = new ASW.DynamoDB.DocumentClient();
    let responseBody = "";
    let statusCode = 0;

try {
        const params = {
            TableName: "demo_sitacomm_chat_messages",
         };
        var allData=new Array;
        await getAllData(params, allData);
        //console.log("Processing Completed");

        console.log("finallenght"+allData.length);
        let messages =allData.sort((a, b) => a.createdat - b.createdat);
        responseBody = JSON.stringify(messages);
         statusCode = 200;
        //console.log(messages);
    } catch(error) {
        console.log(error);
       
        responseBody = error;
        statusCode = 403;
    }
    

    const response = {
        statusCode: statusCode,
        headers: {
            "Content-Type": "aplication/json",
            "Access-Control-Allow-Origin": "*"
        },
        body: responseBody
    };

    return response;
};



const getAllData = async (params, allData) => { 

    console.log("Querying Table");
    //let data = await documentClient.query(params).promise();
    const data = await documentClient.scan(params).promise();
    if(data.Items){
        
       data.Items.forEach(function(item) {
          allData.push(item); 
       });
    }
    //console.log(data.LastEvaluatedKey);
    //return data;/*
   
    if (data.LastEvaluatedKey && allData.length<10000 ) {
        params.ExclusiveStartKey = data.LastEvaluatedKey;
        return await getAllData(params, allData);

    } else {
        return data;
    }
}
