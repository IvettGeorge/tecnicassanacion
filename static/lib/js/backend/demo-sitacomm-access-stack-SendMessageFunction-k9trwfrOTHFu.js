 // Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: MIT-0

const AWS = require('aws-sdk');


const ddb = new AWS.DynamoDB.DocumentClient({ apiVersion: '2012-08-10', region: process.env.AWS_REGION });


const { TABLE_NAME } = process.env;
const  TABLE_DETAILS='demo_sitacomm_access_details';


exports.sendMessage = async event => {
  
	//await createMessageMock();
	// return;
	let postData = JSON.parse(event.body).data;
	let alldata =JSON.parse(postData);
	

	//SE CREA DETALLE CON TODOS LOS DATOS
	const connectionId= event.requestContext.connectionId;
	alldata.connectionId=connectionId;
	alldata.idDetail=connectionId;
	alldata.activo = true;
	try {
		await createDetail(connectionId, alldata); 
	} catch (e) {
		//console.log(e);
		return { statusCode: 500, body: e };
	}

	//SE ENVIA MENSAJE A TODOS LOS SUSCRITOS
  let connectionData;
 
  try {
    connectionData = await ddb.scan({ TableName: TABLE_NAME, ProjectionExpression: 'connectionId' }).promise();
  } catch (e) {
    return { statusCode: 500, body: e.stack };
  }
 
  const apigwManagementApi = new AWS.ApiGatewayManagementApi({
    apiVersion: '2018-11-29',
    endpoint: event.requestContext.domainName + '/' + event.requestContext.stage
  });
 
 
  const postCalls = connectionData.Items.map(async ({ connectionId }) => {
    try {
      await apigwManagementApi.postToConnection({ ConnectionId: connectionId, Data: JSON.stringify(alldata)//postData  
        
      }).promise();
    } catch (e) {
      if (e.statusCode === 410) {
        console.log(`Found stale connection, deleting ${connectionId}`);
        await ddb.delete({ TableName: TABLE_NAME, Key: { connectionId } }).promise();
      } else {
        throw e;
      }
    }
  });
 
  try {
    await Promise.all(postCalls);
  } catch (e) {
    return { statusCode: 500, body: e.stack };
  }


  return { statusCode: 200, body: 'Data sent.' };
};



async function createDetail(connectionId, alldata){
 
  const putParams = {
    TableName: TABLE_DETAILS,
    Item: {
      idDetail 	: ""+connectionId,//""+Date.now(),//
      connectionId 	:connectionId,
      //username 		: alldata.username,
      email			:alldata.email,
      nombre 		:alldata.nombre,      
    
      office        :alldata.office,
      job           :alldata.job,
      region        :alldata.region,
      code          :alldata.code, 
           
      activo : true,
      createdat: ""+Date.now(),
      getoutat: ""+Date.now()
    }
  };

  try {
    await ddb.put(putParams).promise();
  } catch (err) {    
    return { statusCode: 500, body: 'Failed at create mmessages to connect: ' + JSON.stringify(err) };
  }
}

