//$(function() {
function languageChart(values,labelsChart) {
/* ChartJS
   * -------
   * Data and config for chartjs
   */
  'use strict';

  var doughnutPieData = {
    datasets: [{
      data: values,//[28, 0, 0],
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
    }],

    // These labels appear in the legend and in the tooltips when hovering different arcs
    labels: labelsChart
  };

  var doughnutPieOptions = {
    responsive: true,
    animation: {
      animateScale: true,
      animateRotate: true
    }
  };

  if ($("#languageChart").length) {
    var languageChartCanvas = $("#languageChart").get(0).getContext("2d");
    var languageChart = new Chart(languageChartCanvas, {
      type: 'pie',
      data: doughnutPieData,
      options: doughnutPieOptions
    });
  }

}
//);