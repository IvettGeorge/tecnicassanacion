from django.shortcuts import get_object_or_404, redirect, render
from django.contrib.auth.views import LoginView, UserModel
from django.contrib.auth import authenticate, login, logout
from django.views.generic import RedirectView
from django.http import HttpResponse, JsonResponse, request

#Django-Sesame libs
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from sesame import tokens
from sesame.utils import get_user, get_query_string, get_token

from user.models import User
from core.models import Event_User
from login.forms import LoginForm
from django.http import HttpResponseRedirect
from core.models import Event, Event_User

# Create your views here.
class LoginFormView(LoginView):    
    #template_name='login.html'
    template_name='loginlib.html'
    
'''
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            print("Login")
            print(request.user)
            return redirect('/home')
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context= super().get_context_data(**kwargs)
        context['title']='Iniciar Sesión'        
        return context
'''
class LogoutRedirectView(RedirectView):
    pattern_name='login'

    def dispatch(self, request, *args, **kwargs):
        logout(request)
        return super().dispatch(request, *args, **kwargs)


def login_view(request):
    if request.user.is_authenticated:
            return redirect('/home')
    form = LoginForm(request.POST or None)
    if request.POST:
        if form.is_valid():
            user = form.login(request)
            if user:
                login(request, user)
                return HttpResponseRedirect("/home")# Redirect to a success page.
        else:
            print("form no valid")
    else:
        form=LoginForm()
    return render(request, 'loginlib.html', {'form':form})


#Django-Sesame Views
def helloSesame(request, user):
    userLogin=User
    usuario=userLogin.objects.get(email=user)
    query=get_token(usuario)
    if query is None:
        response="User no valid"
    else:
        response =query
    return HttpResponse(response)

def loginSesame(request,token):
    print(token)
    user = get_user('AAAAAX5zd5_cKbR9xOA', update_last_login=True)
    if user is None:
        print("no valid")
    else:
        print(user)
    return HttpResponse("Hello {}!".format(user))

def auto_login(request):
    """URL Auto LoginView"""
    try:
        # Gets URL parameters
        user = request.GET.get('user')
        #password = request.GET.get('password')
        event_id = request.GET.get('event')       
       
        # Check if user match with event

        #event_users=Event_User.objects.get(mail=user, code=event_id)
        event_users=Event_User.objects.filter(mail=user, code=event_id)
       
        event_user=event_users[0]
        user_authenticated= UserModel.objects.get(email=event_user.mail)

        #event_user=get_object_or_404(Event_User, mail=user, code=event_id)       
        # User Authentication
        #user_authenticated = authenticate(username=user, password=password)
                
        # If user is authenticated, logs user in
        if user_authenticated is not None:
            login(request,  user_authenticated)
            return redirect('/home')
            # If user does not authenticate, redirects to LoginView
        else:
            return redirect('/login')

    except Exception as e:
        print(e)
        return redirect('/login')


