from re import X
from django.shortcuts import render, redirect
# Create your views here.

import requests
import time
import psutil
import os
from datetime import datetime

import user_agents
from user_agents.parsers import Browser
from .models import Analytic, CodeAPIIP
from urllib.parse import urlparse
from django.core.paginator import Paginator

from user_agents import parse

import json
from django.http import JsonResponse

from django.views.decorators.csrf import csrf_exempt

import operator
from django.conf import settings

#traffic monitor
def traffic_monitor(request):
    dataSaved = Analytic.objects.all().order_by('-datetime')
    # Getting loadover15 minutes 
    load1, load5, load15 = psutil.getloadavg()
    cpu_usage = int((load15/os.cpu_count()) * 100)
    ram_usage = int(psutil.virtual_memory()[2])
    p = Paginator(dataSaved, 100)
    #shows number of items in page
    totalSiteVisits = (p.count)
    #find unique page viewers & Duration
    pageNum = request.GET.get('page', 1)
    page1 = p.page(pageNum)
    #unique page viewers
    a = Analytic.objects.order_by().values('ip').distinct()
    pp = Paginator(a, 10)
    #shows number of items in page
    unique = (pp.count)
    #update time
    now = datetime.now()
    data = {
        "now":now,
        "unique":unique,
        "totalSiteVisits":totalSiteVisits,
        "cpu_usage": cpu_usage,
        "ram_usage": ram_usage,
        "dataSaved": page1,
    }
    return render(request, 'traffic_monitor.html', data)

#home page
def homeAnalytics(request):
    context=getAnalytics(request)
    return render(request, 'homeMonitor.html',context)


def getAnalytics(request):

    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    #print(ip)
    access_key=CodeAPIIP.objects.all()[:1].get()
    try:
        response = requests.get('http://api.ipstack.com/'+ip+'?access_key='+access_key.code)
        #response = requests.get('http://api.ipstack.com/2806:2f0:9201:7023:2cc3:e32a:e6b2:2c29?access_key=73d7f46922e1d89234b0dc4d1f1d7db7')
        rawData = response.json()    
        if rawData:        
            
            #print(rawData) 
            continent = 'America'#rawData['continent_name']
            country = rawData['country_name']
            capital = rawData['location']['capital'] 
            city = rawData['city']
            codeCountry=rawData['country_code']
            language=rawData['location']['languages'][0]['native']
            country_flag=rawData['location']['country_flag']
            now = datetime.now()
            datetimenow = now#.strftime("%Y-%m-%d %H:%M:%S")
            event=settings.CODE_EVENT

            #User_Agent Browser/OS/Device
            user_string=request.META['HTTP_USER_AGENT']
            user_agent=parse(user_string)
            os=user_agent.os.family
            browser=user_agent.browser.family
            device=user_agent.device.family
            
            if user_agent.is_pc:
                typeDevice="PC"
            elif user_agent.is_mobile:
                typeDevice="Mobile"
            elif user_agent.is_tablet:
                typeDevice="Tablet"
            elif user_agent.is_touch_capable:
                typeDevice="Touch Capable"
            elif user_agent.is_bot:
                typeDevice="Bot"
            else:
                typeDevice="None"
            
            context={'sistemaO':os,'browser':browser, 'os':os,'device':device, 'language':language,'flag':country_flag,
            'ip':ip, 'message':'ok', 'TypeDevice':typeDevice, 'codeCountry':codeCountry, 'country':country,'capital':capital, 'city':city, 'continent': continent,'event':event}        
            saveNow = Analytic(
                continent=continent,
                country=country,
                capital=capital,
                city=city,
                datetime=datetimenow,
                ip=ip,
                browser=browser,
                os=os,
                device=device,
                type_device=typeDevice,
                language=language,
                country_flag=country_flag,
                codeCountry=codeCountry,
                event=event
            )
            saveNow.save()
        else:
            context={'ip':ip, 'message':rawData['error']['info']}
    except:
         context={'ip':ip, 'message':'Codigo no valido'}

    return (context)

def maps(request):
    context={'title':'Mapa de usuarios'}
    return render(request, 'map.html',context)

def maps2(request):
    context={'title':'Mapa de usuarios'}
    return render(request, 'map2.html',context)

#Diccionario Paises
@csrf_exempt
def dictCountry(request):
    data=getDataMap()
    context=data
    return JsonResponse(context, safe=False)

#Diccionario Paises
@csrf_exempt
def graphData(request):
    countries=getParameter("country")
    languages=getParameter("language")
    cities=getParameter("city")
    data={'countries':countries,'languages':languages,'cities':cities}
    context=[data]
    return JsonResponse(context, safe=False)

#Diccionario Tecnologico
@csrf_exempt
def graphDataTech(request):
    oss=getDataTecnologic("os")
    types_device=getDataTecnologic("type_device")
    devices=getDataTecnologic("device")
    browsers=getDataTecnologic("browser")
    data={'oss':oss,'types_device':types_device,'devices':devices,'browsers':browsers}
    context=[data]
    return JsonResponse(context, safe=False)


#Dashboard    
def dashboard(request):
    if not request.user.is_authenticated:
        print(request.user)
        return redirect('/login')
    data=getData()
    languages=getParameter("language")
    cities=getParameter("city")
    context={'data':data,'languages':languages,'cities':cities}
    return render(request, 'dashboardMain.html',context)

#Json Data Map
def getDataMap():
    master=[]
    data=[]
    analytics=Analytic.objects.all()
    for metric in analytics:
        params={"code":metric.codeCountry,"country":metric.country}
        master.append(params)    
    repeat={}
    for aloha in master:
        for r,s in aloha.items():
            if r=="code":
                code=s
            elif r =='country':
                countryDict={}
                if s in repeat :
                    i+=1
                    countryDict['country']=s
                    countryDict['value']=i
                    countryDict['code']=code
                    repeat[s] =countryDict
                else:
                    i=0
                    countryDict['value'] = i
                    repeat[s] = countryDict            
    print(repeat)
    for country,values in repeat.items():
        print(values)
        data.append(values)    
    print(data)
    return (data)

#Function Dashboard
def getData():
    master=[]
    analytics=Analytic.objects.all()
    for metric in analytics:
        params={"code":metric.codeCountry,"language":metric.language,'flag':metric.country_flag,"country":metric.country,'city':metric.capital}
        master.append(params)    
    repeat={}
    for aloha in master:
        for r,s in aloha.items():
            if r=="code":
                code=s
            elif r=="language":
                language=s
            elif r=="flag":
                flag=s            
            elif r=="city":
                city=s            
            elif r =='country':
                countryDict={}
                if s in repeat :
                    i+=1
                    countryDict['country']=s
                    countryDict['language']=language
                    countryDict['value']=i
                    countryDict['code']=code
                    countryDict['flag']=flag
                    countryDict['city']=city
                    repeat[s] =countryDict

                else:
                    i=0
                    countryDict['value'] = i
                    repeat[s] = countryDict            
    listof=[repeat]
    return (listof)

#Get Any Parameter
def getParameter(parameter):
    master=[]
    analytics=Analytic.objects.all()
    for metric in analytics:
        params={"code":metric.codeCountry,"language":metric.language,'flag':metric.country_flag,"country":metric.country,'city':metric.capital}
        master.append(params)    
    repeat={}
    for aloha in master:
        for r,s in aloha.items():
            if r==parameter:
                if s in repeat:
                    i+=1                    
                else:
                    i=0
                repeat[s]=i
    paramsOrder = sorted(repeat.items(), key=operator.itemgetter(1), reverse=True)        
    myDict=toDict(paramsOrder)
    return (myDict)

#Convert list to Dictionary
def toDict(list):
    myDict={}
    for element in list:
        if element in myDict:
            myDict[element[0]]=element[1]           
        else:
            myDict[element[0]]=element[1]           
    return myDict

def getDataTecnologic(parameter):
    master=[]
    analytics=Analytic.objects.all()
    for metric in analytics:
        params={"os":metric.os,"device":metric.device,'type_device':metric.type_device,"browser":metric.browser}
        master.append(params)    
    repeat={}
    for aloha in master:
        for r,s in aloha.items():
            if r==parameter:
                if s in repeat:
                    i+=1                    
                else:
                    i=0
                repeat[s]=i
    paramsOrder = sorted(repeat.items(), key=operator.itemgetter(1), reverse=True)        
    myDict=toDict(paramsOrder)
    return (myDict)

def tecnologics(request):
    if not request.user.is_authenticated:
        print(request.user)
        return redirect('/login')
    oss=getDataTecnologic("os")
    devices=getDataTecnologic("device")
    type_device=getDataTecnologic('type_device')
    browser=getDataTecnologic("browser")
    context={'os':oss,'devices':devices,'type_devices':type_device,'browsers':browser}
    return render(request, 'dashboardTecnologics.html',context)